# Mixie mobile webview showcase application

**NOTE: The sample code uses the EasyPermissions library for simple permission management. To install this library in your project please see [EasyPermissions installation guide](https://github.com/googlesamples/easypermissions)**

This application contains a single MainActivity. This main activity host a full screen WebView. The layout is very simple:

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <WebView
        android:id="@+id/web_view"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

</androidx.constraintlayout.widget.ConstraintLayout>
```

First bind the WebView in your activity:

```java
webView = (WebView) findViewById(R.id.web_view);
```

Create a new ```WebViewSetting``` and set the following to enable javascript execution, local storage and camera usage for the WebView:

```java
WebSettings settings = webView.getSettings();
settings.setJavaScriptEnabled(true);
settings.setDomStorageEnabled(true);
settings.setDatabaseEnabled(true);
settings.setMediaPlaybackRequiresUserGesture(false);
```

Create a new WebChromeClient for the WebView and override the following methods:

```java
webView.setWebChromeClient(new WebChromeClient() {

            /** Optional, to print console messages in LogCat */
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.d(MainActivity.class.getName(), consoleMessage.sourceId() + "\t" + consoleMessage.lineNumber() + ": " + consoleMessage.message());
                return true;
            }

            /** Runtime permission management for camera access */
            @Override
            public void onPermissionRequest(PermissionRequest request) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void run() {
                        request.grant(request.getResources());
                    }
                });
            }

        });
```

**NOTE: Please avoid using WebViewClient for WebView! If you using WebViewClient, you need to override the following method for open external link in the browser instead of inside the WebView. If you are using WebViewClient, please migrate to WebChromeClient as soon as possible.**

Implementation for WebViewClient:

```java
webView.setWebViewClient(new WebViewClient() {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        // Load embedded web-application
        if(url.equals(APP_URL)){
            return false;
        }

        // Load the other urls in browser app
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        view.getContext().startActivity(intent);
        return true;
    }
});
```

Add the following permissions to your manifest file:

```xml
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.CAMERA" />
<uses-permission android:name="android.webkit.PermissionRequest" />
```

For proper camera handling add the following ```uses-features```:

```xml
<uses-feature android:name="android.hardware.camera" />
<uses-feature android:name="android.hardware.camera.autofocus" />
```

The complete code of the MainActivity:

```java
public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    private static final int RC_CAMERA_PERM = 123;
    private static final String APP_URL = "your-app-url-here";
    WebView webView;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = (WebView) findViewById(R.id.web_view);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setMediaPlaybackRequiresUserGesture(false);

        EasyPermissions.requestPermissions(
                this,
                getString(R.string.rationale_camera),
                RC_CAMERA_PERM,
                Manifest.permission.CAMERA);

        /*
         * Deprecated implementation with WebViewClient
         * Use WebChromeClient
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // Load embedded web-application
                if(url.equals(APP_URL)){
                    // load my page
                    return false;
                }

                // Open other urls in the browser app
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                view.getContext().startActivity(intent);
                return true;
            }
        });
        */

        webView.setWebChromeClient(new WebChromeClient() {

            /** Optional, to print console messages in LogCat */
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.d(MainActivity.class.getName(), consoleMessage.sourceId() + "\t" + consoleMessage.lineNumber() + ": " + consoleMessage.message());
                return true;
            }

            /** Runtime permission management for camera access */
            @Override
            public void onPermissionRequest(PermissionRequest request) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void run() {
                        request.grant(request.getResources());
                    }
                });
            }

        });


    }

    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void loadView() {
        if (hasCameraPermission()) {
            webView.loadUrl(APP_URL);
        } else {
            EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.rationale_camera),
                    RC_CAMERA_PERM,
                    Manifest.permission.CAMERA);
        }

    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        Log.d(MainActivity.class.getName(), "onPermissionsGranted:" + requestCode + ":" + perms.size());
        webView.loadUrl(APP_URL);
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        EasyPermissions.requestPermissions(
                this,
                getString(R.string.rationale_camera),
                RC_CAMERA_PERM,
                Manifest.permission.CAMERA);
    }

    private boolean hasCameraPermission() {
        return EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA);
    }
}
```

## Create Deep Link in the application

Before you start please take a quick look on the [Create Deep Links to App Content](https://developer.android.com/training/app-links/deep-linking) page.

To create a link to your Activity, add the following intent filter inside your Activity in your Manifest file:

```xml
<intent-filter android:label="deepLink">
    <action android:name="android.intent.action.VIEW" />
    <category android:name="android.intent.category.DEFAULT" />
    <category android:name="android.intent.category.BROWSABLE" />
    <data
        android:host="mixie"
        android:scheme="mobile" />
</intent-filter>
```

This deep link will bind to the following URL

```url
intent://mixie/#Intent;scheme=mobile;package=com.mixie.mixinewv;end
```

With the `intent://` prefix, when the application isn't installed on the device, Google Play store should open automatically.

You can test the application showcase on this page: https://mixie.luna-space.com/

### Read query parameter from deep link

When you want to receive extra parameters from the deep link, you can read them from the intent data. In the example, we read the `redirectTo` parameter from the deep link and pass it to the url for the WebView.

First get the URI from the Intent:

```java
Uri appLinkData = getIntent().getData();
```

Then check it is not null and read the query parameter. You can to this in the `onCreate` method:

```java
if (appLinkData != null) {
    String redirectTo = appLinkData.getQueryParameter("redirectTo");
    handleUrl = APP_URL + URLEncoder.encode(redirectTo);
} else {
    handleUrl = APP_URL + DEFAULT_REDIRECT;
}
```

Load the `handleUrl` instead of the `APP_URL`:

```java
webView.loadUrl(handleUrl);
```

Now our deep link looks like this:
`intent://mixie?redirectTo=/scanner#Intent;scheme=mobile;package=com.mixie.mixinewv;end`
