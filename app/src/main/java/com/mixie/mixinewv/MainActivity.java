package com.mixie.mixinewv;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.annotation.TargetApi;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.net.URLEncoder;
import java.util.List;

import com.mixie.mixinewv.R;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    private static final int RC_CAMERA_PERM = 123;
    private static final String APP_URL = "https://app-testing.mixiedigital.com/token?data=eyJhbGciOiJIUzI1NiIsImRpc2NyaW1pbmF0b3IiOiJTSU1QTEUifQ.eyJzdWIiOiJ7XG4gIFwiZW1haWxcIjogXCJjc2ludGFsYW4uYmFsYXpzK2ludGVncmF0ZWRwb255dmlzaXRvckBkYW51Yml1c2luZm8uaHVcIixcbiAgXCJhdXRoZW50aWNhdGlvbk93bmVyRW1haWxcIjogXCJjc2ludGFsYW4uYmFsYXpzK3Byb3VkcG9uaWVzQGRhbnViaXVzaW5mby5odVwiXG59IiwiaXNzIjoibWl4aWVkaWdpdGFsLmNvbSIsImV4cCI6MTY1NDc4NDc0MywiaWF0IjoxNjQ3MDA4NzQzfQ.VuZp2HfPC0o4VXSJ1m2w5VOH01OlpqxX6Yke7yBPhBU&redirectTo=";
    private static final String DEFAULT_REDIRECT = "%2Fscanner";
    private String handleUrl;
    WebView webView;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Uri appLinkData = getIntent().getData();

        if (appLinkData != null) {
            String redirectTo = appLinkData.getQueryParameter("redirectTo");
            if (redirectTo != null) {
                handleUrl = APP_URL + URLEncoder.encode(redirectTo);
            } else {
                handleUrl = APP_URL + DEFAULT_REDIRECT;
            }
        } else {
            handleUrl = APP_URL + DEFAULT_REDIRECT;
        }


        webView = (WebView) findViewById(R.id.web_view);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setMediaPlaybackRequiresUserGesture(false);

        EasyPermissions.requestPermissions(
                this,
                getString(R.string.rationale_camera),
                RC_CAMERA_PERM,
                Manifest.permission.CAMERA);

        /*
         * Deprecated implementation with WebViewClient
         * Use WebChromeClient
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // Load embedded web-application
                if(url.equals(APP_URL)){
                    // load my page
                    return false;
                }

                // Open other urls in the browser app
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                view.getContext().startActivity(intent);
                return true;
            }
        });
        */

        webView.setWebChromeClient(new WebChromeClient() {

            /** Optional, to print console messages in LogCat */
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.d(MainActivity.class.getName(), "CONSOLE: " + consoleMessage.sourceId() + "\t" + consoleMessage.lineNumber() + ": " + consoleMessage.message());
                return true;
            }

            /** Runtime permission management for camera access */
            @Override
            public void onPermissionRequest(PermissionRequest request) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void run() {
                        request.grant(request.getResources());
                    }
                });
            }

        });

        webView.setWebViewClient(new WebViewClient() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Nullable
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
                Log.d("NETWORK REQ", "Request: " + request.getRequestHeaders());
                Log.d("NETWORK REQ", "Request URL: " + request.getUrl());

                return null;
            }
        });

    }

    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void loadView() {
        if (hasCameraPermission()) {
            webView.loadUrl(handleUrl);
        } else {
            EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.rationale_camera),
                    RC_CAMERA_PERM,
                    Manifest.permission.CAMERA);
        }

    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        Log.d(MainActivity.class.getName(), "onPermissionsGranted:" + requestCode + ":" + perms.size());
        webView.loadUrl(handleUrl);
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        EasyPermissions.requestPermissions(
                this,
                getString(R.string.rationale_camera),
                RC_CAMERA_PERM,
                Manifest.permission.CAMERA);
    }

    private boolean hasCameraPermission() {
        return EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA);
    }
}